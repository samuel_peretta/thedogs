# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

# ruby encoding: utf-8

Dog.delete_all # For avoiding duplicate content


Dog.create(breed: 'Bull Terrier', age: 2, coat: "branco", sex: "fêmea", name: "Lola", price: "R$ 500,00", 
  image_file_name: "bullterrier.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
Dog.create(breed: 'Bulldog', age: 1, coat: "marrom", sex: "macho", name: "Brutus", price: "R$ 500,00", 
  image_file_name: "bulldog.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
Dog.create(breed: 'Komodor', age: 1, coat: "branco", sex: "macho", name: "Brutus", price: "R$ 500,00", 
  image_file_name: "komodor.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
Dog.create(breed: 'Yorkshire', age: 3, coat: "preto", sex: "fêmea", name: "Lola", price: "R$ 500,00", 
  image_file_name: "yorkshire.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
Dog.create(breed: 'Rottweiler', age: 1, coat: "preto", sex: "macho", name: "Brutus", price: "R$ 500,00", 
  image_file_name: "rottweiler.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
Dog.create(breed: 'Peekapoo', age: 3, coat: "branco", sex: "fêmea", name: "Lola", price: "R$ 500,00", 
  image_file_name: "peekapoo.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
Dog.create(breed: 'Akita', age: 2, coat: "branco", sex: "macho", name: "Brutus", price: "R$ 500,00", 
  image_file_name: "akita.jpg", image_content_type: "image/jpeg", image_file_size: 42, image_updated_at: "2015-01-29 07:54:52")
