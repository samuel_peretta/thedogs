class CreateDogs < ActiveRecord::Migration
  def change
    create_table :dogs do |t|
        t.string  :breed
        t.integer  :age
        t.string :name
        t.string  :coat
        t.string :sex
        t.string  :price
        t.attachment :image
        t.timestamps
    end
  end
  def self.down
    drop_table :dogs
  end
end
