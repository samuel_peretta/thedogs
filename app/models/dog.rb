class Dog < ActiveRecord::Base
  
  attr_accessible :breed, :age, :name, :coat, :sex, :price, :image, :image_file_name, :image_content_type, :image_file_size, :image_updated_at

  include PgSearch 

  multisearchable :against => [:breed, :sex, :name ]

  has_attached_file :image

  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates_presence_of :breed, :price

  def self.tsearch_query(search_terms, limit = query_limit)
    Dog.from("dogs, to_tsquery('pg_catalog.english') as q").where("breed @@ q").limit(limit)
  end

  # Selects search results with plain text title & body columns.
  # Select columns are explicitly listed to avoid returning the long redundant tsv strings
  def self.plain_tsearch(search_terms, limit = query_limit)
    select([:breed, :name, :sex]).tsearch_query(search_terms, limit)
  end

  # Select search results with HTML highlighted title & body columns
  def self.highlight_tsearch(search_terms, limit = query_limit)
    body = "ts_headline(body, q, 'StartSel=<em>, StopSel=</em>, HighlightAll=TRUE') as body"
    title = "ts_headline(title, q, 'StartSel=<em>, StopSel=</em>, HighlightAll=TRUE') as title"
    Dog.select([body, title, :id]).tsearch_query(search_terms, limit)
  end

  def self.query_limit; 25; end

  def image_url
    self.image.url(:thumb)
  end

end
