class HomeController < ApplicationController
  def index
    @dogs = Dog.all
  end

  def show
    @dogs = Dog.find(params[:id])
  end

  def search
 
    @dogs = Dog.plain_tsearch(params[:search])
   
    respond_to do |format|
      format.html { render :index }
      format.js { @dogs }
    end
  end

end
